# Expivi Theme Package

This repo contains the build logic for newer themes.

## Usage

In your `webpack.mix.js` import the package like so.

```js
const mix = require('@expivi/theme-package')(require('laravel-mix'));
```
