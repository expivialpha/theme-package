const ThemeJson = require('./src/theme_json');
const SettingsJson = require('./src/settings_json');
const Zip = require('./src/zip');
const Cleanup = require('./src/cleanup');

module.exports = function (mix) {
    mix.options({ clearConsole: false, manifest: false })
        .disableNotifications()
        .setPublicPath('dist/assets')
        .setResourceRoot('..');

    mix.before(() => Cleanup.run(mix));
    mix.after(() => {
        ThemeJson.run(mix);
        console.log(); // Empty line
        SettingsJson.run(mix);
        console.log(); // Empty line
    });
    mix.then(() => Zip.run(mix));

    return mix;
};
