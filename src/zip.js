const archiver = require('archiver');
const fs = require('fs');
const { filesize } = require('filesize');
const Plugin = require('./base/plugin');

module.exports = class Zip extends Plugin {
    static title = 'Zip';

    static run(mix) {
        return new Promise((resolve, reject) => {
            const themeEnv = process.env.THEME_ENV ?? 'production';
            const nodeEnv = process.env.NODE_ENV === 'production' ? 'prod' : 'dev';
            const filename = `theme.${themeEnv}.${nodeEnv}.zip`;

            const output = fs.createWriteStream(`./${filename}`);
            const archive = archiver('zip', { zlib: { level: 9 } });

            archive.on('finish', () => {
                const size = filesize(archive.pointer(), { fullform: true });

                this.success(`Compressed into ${filename} of ${size}`);

                resolve();
            });

            archive.on('error', error => {
                this.error(`Failed to compress files: ${error.message}`);

                reject(error);
            });

            archive.pipe(output);
            archive.directory('./dist', false);
            archive.finalize();
        });
    }
};
