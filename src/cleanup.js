const fs = require('fs');
const Plugin = require('./base/plugin');

module.exports = class Cleanup extends Plugin {
    static title = 'Cleanup';

    static run(mix) {
        const distFolder = './dist';

        if (fs.existsSync(distFolder)) {
            fs.rmdirSync(distFolder, { recursive: true });
        }

        fs.mkdirSync(`${distFolder}/assets`, { recursive: true });
        fs.mkdirSync(`${distFolder}/views`);
        fs.mkdirSync(`${distFolder}/lang`);

        this.success('Cleaned dist folder');
    }
};
