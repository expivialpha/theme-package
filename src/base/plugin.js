const chalk = require('chalk');

module.exports = class Plugin {
    static title = 'Plugin';

    static run(mix) {
        throw new Error('Implement me');
    }

    static success(message) {
        console.log(chalk.green(`✔ ${this.title}`));
        console.log(chalk.grey(`  ${message}`));
    }

    static warning(message) {
        console.log(chalk.yellow(`⚠ ${this.title}`));
        console.log(chalk.grey(`  ${message}`));
    }

    static error(message) {
        console.log(chalk.red(`✖ ${this.title}`));
        console.log(chalk.grey(`  ${message}`));
    }
}
