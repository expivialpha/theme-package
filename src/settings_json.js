const fs = require('fs');
const Plugin = require('./base/plugin');

module.exports = class Theme extends Plugin {
    static title = 'Settings.json';

    static run(mix) {
        const filename = './settings.json';
        const fileExists = fs.existsSync(filename);

        if (fileExists) {
            const theme = JSON.parse(fs.readFileSync(filename, 'utf8'));

            fs.writeFileSync('./dist/settings.json', JSON.stringify(theme, null, 4));
        }

        this.success(fileExists ? 'File included' : 'File not found');
    }
};
