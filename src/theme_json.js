const fs = require('fs');
const Plugin = require('./base/plugin');

module.exports = class Theme extends Plugin {
    static title = 'Theme.json';

    static run(mix) {
        const themeFilename = './theme.json';

        if (!fs.existsSync(themeFilename)) {
            throw new Error('theme.json is missing!');
        }

        if (mix.inProduction()) {
            Theme.bumpVersion(themeFilename);
        }

        let theme = JSON.parse(fs.readFileSync(themeFilename, 'utf8'));

        /** @type {boolean|null} */
        let includeSuccess = null;

        // If in dev load, load theme.dev.json over the theme.json
        if (!mix.inProduction()) {
            const themeEnvFilename = `./theme.dev.json`;

            includeSuccess = fs.existsSync(themeEnvFilename);

            if (includeSuccess) {
                theme = {
                    ...theme,
                    ...JSON.parse(fs.readFileSync(themeEnvFilename, 'utf8')),
                };
            }
        }

        fs.writeFileSync('./dist/theme.json', JSON.stringify(theme, null, 4));

        if (includeSuccess === true) {
            this.success('Compiled with theme.dev.json');
        } else if (includeSuccess === false) {
            this.warning('Could not find theme.dev.json');
        } else {
            this.success('Compiled successfully');
        }
    }

    static bumpVersion(themeFilename) {
        const theme = JSON.parse(fs.readFileSync(themeFilename, 'utf8'));

        if (!theme.version || typeof theme.version !== 'string') {
            return;
        }

        const version = theme.version.split('.').map(Number);

        version[version.length - 1]++;

        theme.version = version.join('.');

        fs.writeFileSync(themeFilename, JSON.stringify(theme, null, 4) + '\n'); // Add extra end of line
    }
};
